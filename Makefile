all: Principal.o Descritor.o Teste.o
	gcc Principal.o Descritor.o Teste.o -o saida

Principal.o: 
	gcc -c Principal.c
	
Descritor.o: 
	gcc -c Descritor.c
	
Teste.o: 
	gcc -c Teste.c
	
clean:
	rm -Rf *.o saida
