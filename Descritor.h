#ifndef Descritor_x_123

#include "Node.h"

typedef struct {
    PNo topo;
    PNo fim;
    int length;
} Descritor;

typedef Descritor *PDesc;

PDesc criar();
int inserirFim(PDesc, PDado);
int inserirInicio(PDesc, PDado);
int inserirPos(PDesc, PDado, int);
PNo buscaEndereco(PDesc, PDado);
int buscaChave(PDesc, int);
int removerChave(PDesc, int);

#define Descritor_x_123
#endif
